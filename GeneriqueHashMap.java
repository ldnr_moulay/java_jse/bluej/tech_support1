import java.util.HashMap;

/**
 * Décrivez votre classe HashMap ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class GeneriqueHashMap<t,s>
{
    // variables d'instance
    private t key;
    private s value;

    /**
     * Constructeur d'objets de classe HashMap
     */
    public GeneriqueHashMap()
    {
        this.key=null;
        this.value=null;     
    }
    
    public GeneriqueHashMap(t key, s value)
    {
        this.key=key;
        this.value=value;
    }
    public void setValeurs(t key, s value){
        this.key = key;
        this.value=value;
    }
    
    public void setKey(t key){
        this.key = key;
    }
    
    public void setValue(s value){
        this.value = value;
    }
    
    public s getValue(){
        return value;
    }
    
    public t getKey(){
        return key;
    }
}
