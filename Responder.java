import java.util.ArrayList;
import java.util.Random;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * The responder class represents a response generator object.
 * It is used to generate an automatic response to an input string.
 * 
 * @author     Michael Kölling and David J. Barnes
 * @version    0.1 (2016.02.29)
 */
public class Responder
{
    private Random rand = new java.util.Random();
    private InputReader reader = new InputReader();
    
    private List<String> reponsesHasard = new ArrayList<String>();
    private Map<String, String> reponsesAdapte = new HashMap<String, String>();
    /**
     * Construct a Responder - nothing to do
     */
    public Responder()
    {
            fillResponseAdapte();
            fillResponseHasard();
    }
    /**
     * Genere une response au hasard.
     */
    private String generateResponseHasard()
    { 
        int randomIndex = (int) (Math.random() * reponsesHasard.size());
        return reponsesHasard.get( randomIndex );
    }
        /**
     * Rempli la liste de reponses aux hasard.
     */
    private void fillResponseHasard()
    {
        reponsesHasard.add("Je sais pas");
        reponsesHasard.add("Ce n'est pas mon problème");
        reponsesHasard.add("Demande au voisin");
        reponsesHasard.add("Pourquoi tu me demande à moi ?");
        reponsesHasard.add("Je m'en fiche");
    }
        /**
     * Genere une reponse adapté en fonction de la clé.
     * @return   A string that should be displayed as the response
     */
    private String generateResponseAdapte(String key)
    {
        return reponsesAdapte.get(key);
    }
           /**
     * Rempli la map de reponses adaptées.
     */
    private void fillResponseAdapte()
    {
        reponsesAdapte.put("bug", "Je bug jamais mec");
        reponsesAdapte.put("crash", "Je crash jamais mec");
    }
    
    public void generateResponse(String input){
        if(reponsesAdapte.containsKey(input)){
                String responseAdapte = generateResponseAdapte(input);
                System.out.println(responseAdapte);
            }
        else{
                String responseHasard = generateResponseHasard();
                System.out.println(responseHasard);
            }
    }
}
