/**
 * This class implements a technical support system. It is the top level class 
 * in this project. The support system communicates via text input/output 
 * in the text terminal.
 * 
 * This class uses an object of class InputReader to read input from the user,
 * and an object of class Responder to generate responses. It contains a loop
 * that repeatedly reads input and generates output until the users wants to 
 * leave.
 * 
 * @author     Michael Kölling and David J. Barnes
 * @version    0.1 (2016.02.29)
 */
public class SupportSystem
{
    private Responder responder;
    private InputReader reader = new InputReader();
    
    /**
     * Creates a technical support system.
     */
    public SupportSystem()
    {
        responder = new Responder();
    }

    /**
     * Start the technical support system. This will print a welcome
     * message and enter into a dialog with the user, until the user
     * ends the dialog.
     */
    public void start()
    {
        boolean finished = false;

        printWelcome();

        while(!finished) {
            String input = reader.getInput();
            if(input.contains("bye")) {
                finished = true;
            }
            else {
                 responder.generateResponse(input);
            }
        }

        printGoodbye();
    }

    /**
     * Print a welcome message to the screen.
     */
    private void printWelcome()
    {
        System.out.println("Bienvenue sur le système de support technique de DodgySoft Technical.");
        System.out.println();
        System.out.println("S'il-vous-plait décrivrez votre problème.");
        System.out.println("Nous allons essayer de vous aider au mieux.");
        System.out.println("Tapez 'bye' pour quitter.");
    }

    /**
     * Print a good-bye message to the screen.
     */
    private void printGoodbye()
    {
        System.out.println("Au plaisir de vous revoir...");
    }
}
